#include "../component/keyboard.hpp"
#include "../component/robot.hpp"
#include "../component/ui.hpp"

typedef enum { SELECT_INFANTRY, SELECT_HERO, NOT_SELECT } Game_User_Select_t;

class Game : public KeyBoard, UI {
 public:
  Game();
  ~Game();
  void Start();
  void Refresh();
  bool Key_Scan();
  void Hit_Scan();
  void Show(Robot* robot, Config_Color_t color);

 private:
  Game_User_Select_t red;
  Game_User_Select_t blue;
  Robot* Robot_Red[Robot_Number];
  Robot* Robot_Blue[Robot_Number];
};
