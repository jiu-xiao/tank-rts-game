#include <iostream>

#include "./game/game.hpp"

int main() {
  Game game;
  game.Start();
  return 0;
}
