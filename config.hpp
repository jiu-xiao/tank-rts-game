#ifndef __CONFIG_HPP__

#define __CONFIG_HPP__

#define SCREEN_HEIGHT (1080)
#define SCREEN_WIDTH (1920)
#define SINGLE_AREA_LENGTH (60)
#define INFANTRY_NUMBER (2)
#define HERO_NUMBER (1)
#define SENTRY_NUMBER (1)
#define BASE_NUMBER (1)

typedef enum { Base, Hero, Infantry, Sentry, Robot_Number } Config_Robot_Name_t;

typedef enum {
  BLUE_FORE = 0,
  BLUE_BACK,
  BLUE_LEFT,
  BLUE_RIGHT,
  BLUE_SELECT_HERO,
  BLUE_SELECT_INFANTRY,
  RED_FORE,
  RED_BACK,
  RED_LEFT,
  RED_RIGHT,
  RED_SELECT_HERO,
  RED_SELECT_INFANTRY,
  Quit,
  KEY_BEHAVIOR_NUMBER,
  KEY_BEHAVIOR_NULL
} Config_Key_Behavior_t;

typedef enum { CONFIG_COLOR_RED, CONFIG_COLOR_BLUE } Config_Color_t;

typedef struct {
  float x;
  float y;
} Address_t;

#endif
