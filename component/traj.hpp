#include <list>

#include "../config.hpp"

typedef struct {
  Address_t origin;

  Address_t target;

  bool enable;

  int time;
} Traj_t;

class Traj {
 public:
  Traj();

  void Start(Address_t* o, Address_t* t);

  bool Died();

  void Stop();

  bool Traj_Available();

  Traj_t traj;
};
