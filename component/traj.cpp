#include "traj.hpp"

#include <math.h>

#define TRAJ_CATCH (30)

Traj::Traj() { traj.enable = 0; }

void Traj::Start(Address_t* o, Address_t* t) {
  traj.time = 0;
  traj.origin.x = o->x;
  traj.origin.y = o->y;
  traj.target.x = t->x;
  traj.target.y = t->y;
  traj.enable = 1;
}

bool Traj::Died() {
  return (sqrt((traj.origin.x - traj.target.x) *
                   (traj.origin.x - traj.target.x) +
               (traj.origin.y - traj.target.y) *
                   (traj.origin.y - traj.target.y)) /
          TRAJ_CATCH) < traj.time;
}

void Traj::Stop() { traj.enable = 0; }

bool Traj::Traj_Available() { return traj.enable; }
