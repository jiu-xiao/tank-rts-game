#include "../config.hpp"

class UI {
 public:
  UI();

  void Clear();

  void Test();

  void Bullet_Casing(Address_t *origin, Address_t *target, int time);

  void UI_Show(Config_Robot_Name_t robot, Config_Color_t color,
               Address_t *address, Address_t *target, float angle,
               float HP_Range);
  void UI_Start();

  void UI_End(Config_Color_t color);

  void Delay();
};
