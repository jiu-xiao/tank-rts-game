#include "../config.hpp"
#include "traj.hpp"

typedef enum { Scan_Not_Found, Scan_Detect, Scan_Hit } Robot_ScanAns_t;

typedef struct {
  Address_t address;

  Address_t target;

  float HP;

  float angle;

  float Attack_heat;
} Robot_Status_t;

typedef struct {
  float HP_MAX;
  float Attack_Range;
  float Detection_Range;
  float Damage;
  float Attack_Speed;
  float Speed;
  Config_Robot_Name_t Name;
} Robot_Param_t;

class Robot : public Traj {
 public:
  Robot(Config_Robot_Name_t name, Config_Color_t color);

  void Place(float x, float y);

  Robot_ScanAns_t Scan(Robot* target);

  void Hited(float damage);

  void Hit(Robot* robot);

  void SetTarget(float x, float y);

  void ChangeTarget(float x, float y);

  void Move();

  float Distance(Robot* robot);

  Config_Robot_Name_t Name();

  Address_t* Address();

  Address_t* Target();

  float Angle();

  float HP();

  bool Life();

 private:
  Robot_Status_t status;
  Robot_Param_t param;
};
