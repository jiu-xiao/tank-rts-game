#include "../config.hpp"

class KeyBoard {
 public:
  KeyBoard();
  Config_Key_Behavior_t Scan();

 private:
  char key_map[KEY_BEHAVIOR_NUMBER];
};
