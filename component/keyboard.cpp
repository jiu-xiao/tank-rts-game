#include "keyboard.hpp"

#include <graphics.h>
KeyBoard::KeyBoard() {
  key_map[BLUE_BACK] = 'S';
  key_map[BLUE_FORE] = 'W';
  key_map[BLUE_LEFT] = 'A';
  key_map[BLUE_RIGHT] = 'D';
  key_map[BLUE_SELECT_HERO] = 'H';
  key_map[BLUE_SELECT_INFANTRY] = 'I';
  key_map[RED_BACK] = 0x28;
  key_map[RED_FORE] = 0x26;
  key_map[RED_LEFT] = 0x25;
  key_map[RED_RIGHT] = 0x27;
  key_map[RED_SELECT_HERO] = '+';
  key_map[RED_SELECT_INFANTRY] = '-';
  key_map[Quit] = '\t';
}

Config_Key_Behavior_t KeyBoard::Scan() {
  if (!kbhit()) return KEY_BEHAVIOR_NULL;
  key_msg t = getkey();
  for (int i = 0; i < KEY_BEHAVIOR_NUMBER; i++) {
    if (key_map[i] == t.key) return (Config_Key_Behavior_t)i;
  }
  return KEY_BEHAVIOR_NULL;
}
